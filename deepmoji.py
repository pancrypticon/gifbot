import requests, sys, urllib.parse, urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

reactions = [
    "😂",
    "😒",
    "😩",
    "😭",
    "😍",
    "😔",
    "👌",
    "😊",
    "❤",
    "😏",
    "😁",
    "🎶",
    "😳",
    "💯",
    "😴",
    "😌",
    "☺",
    "🙌",
    "💕",
    "😑",
    "😅",
    "🙏",
    "😕",
    "😘",
    "♥",
    "😐",
    "💁",
    "😞",
    "🙈",
    "😫",
    "✌",
    "😎",
    "😡",
    "👍",
    "😢",
    "😪",
    "😋",
    "😤",
    "✋",
    "😷",
    "👏",
    "👀",
    "🔫",
    "😣",
    "😈",
    "😓",
    "💔",
    "♡",
    "🎧",
    "🙊",
    "😉",
    "💀",
    "😖",
    "😄",
    "😜",
    "😠",
    "🙅",
    "💪",
    "👊",
    "💜",
    "💖",
    "💙",
    "😬",
    "✨",
]


def get_deepmoji_reaction(text, confidence=0.075):
    query = urllib.parse.quote(text, safe="")
    try:
        r = requests.get(
            f"https://deepmoji.mit.edu/api?q={query}", timeout=10, verify=False
        )

        if r.ok:
            data = r.json()
            scores = data["scores"]
            hi_score = 0.0
            hi_index = 0
            for i in range(0, len(scores)):
                if scores[i] > hi_score:
                    hi_score = scores[i]
                    hi_index = i
            if hi_score >= confidence:
                return reactions[hi_index]
    except requests.exceptions.SSLError:
        pass
