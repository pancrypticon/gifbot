FROM python:3-slim

RUN apt update && apt install -y build-essential libolm3 libolm-dev && apt-get clean

COPY * /opt/gifbot/

WORKDIR /opt/gifbot

RUN pip install --no-cache-dir -r requirements.txt

COPY docker-run.sh .

CMD "./docker-run.sh"
