from chat_functions import (
    send_text_to_room,
    mangle_giphy_url,
    fetch_file,
    fetch_ogdata,
    format_ogdata,
    react_to_message,
    get_random_emoji,
)
from deepmoji import get_deepmoji_reaction
from PIL import Image
import logging, os, random, re, requests

logger = logging.getLogger(__name__)


class Message(object):
    def __init__(self, client, store, config, message_content, room, event):
        """Initialize a new Message

        Args:
            client (nio.AsyncClient): nio client used to interact with matrix

            store (Storage): Bot storage

            config (Config): Bot configuration parameters

            message_content (str): The body of the message

            room (nio.rooms.MatrixRoom): The room the event came from

            event (nio.events.room_events.RoomMessageText): The event defining the message
        """
        self.client = client
        self.store = store
        self.config = config
        self.message_content = message_content
        self.room = room
        self.event = event

    async def process(self):
        # omit any quoted content to prevent links in quots from being processed
        pre_match = re.match("(>.*\n)?(.*)", self.message_content)
        message = pre_match.group(2)

        m = re.search("(https?://.*)", message)
        if m and not re.search("#nogif", message):
            url = m.group(1)
            if re.search("\.(gif|png|jpe?g|webp)$", url, re.IGNORECASE):
                link = url
            else:
                link = mangle_giphy_url(m.group(1))
            # if this is a link, but not recognized as an image, try to get the
            # opengraph data and display a preview if it exists
            if link is None:
                og = fetch_ogdata(url)
                logger.debug(og)
                if "image" in og:
                    logger.info(
                        f"Uploading {og['image']} to {self.room.name} ({self.room.room_id})"
                    )
                    await self._upload_image(og["image"])
                output = format_ogdata(og)
                if len(output):
                    logger.info(
                        f"Posting preview for {url} to  {self.room.name} ({self.room.room_id})"
                    )
                    await send_text_to_room(
                        self.client,
                        self.room.room_id,
                        format_ogdata(og),
                        markdown_convert=True,
                    )
            # else if link IS an image, upload it
            else:
                logger.info(
                    f"Uploading {link} to {self.room.name} ({self.room.room_id})"
                )
                await self._upload_image(link)

        # randomly react to the message
        if random.random() <= self.config.config.get("random_reaction_chance", 0.05):
            dm_reaction = (
                get_deepmoji_reaction(self.message_content)
                if self.config.config.get("use_deepmoji", False)
                else None
            )
            emoji = dm_reaction if dm_reaction is not None else get_random_emoji()
            logger.debug(
                f"Reacting to {self.event.event_id} in {self.room.room_id} with {emoji}"
            )
            await react_to_message(
                self.client, self.room.room_id, self.event.event_id, emoji
            )

    async def _upload_image(self, link):
        file, content_type = fetch_file(link)
        if file is not None:
            filename = link.split("/")[-1]
            resp, keys = await self.client.upload(
                file, content_type=content_type, encrypt=True
            )
            size = file.tell()
            image = Image.open(file)
            x, y = image.size
            await self.client.room_send(
                room_id=self.room.room_id,
                ignore_unverified_devices=True,
                message_type="m.room.message",
                content={
                    "body": filename,
                    "info": {
                        "h": y,
                        "w": x,
                        "mimetype": content_type,
                        "size": size,
                    },
                    "msgtype": "m.image",
                    "file": {
                        "url": resp.content_uri,
                        "key": keys["key"],
                        "iv": keys["iv"],
                        "hashes": keys["hashes"],
                        "v": keys["v"],
                    },
                },
            )
        else:
            logger.warning(f"Failed to fetch {link}")

    # os.unlink(filename)
