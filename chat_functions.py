import emojis, logging, opengrapher, random, re, requests
from nio import SendRetryError
from markdown import markdown
from tempfile import TemporaryFile


logger = logging.getLogger(__name__)

USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36"


async def react_to_message(client, room_id, event_id, reaction):
    content = {
        "m.relates_to": {
            "rel_type": "m.annotation",
            "event_id": event_id,
            "key": reaction,
        }
    }
    try:
        await client.room_send(
            room_id,
            "m.reaction",
            content,
            ignore_unverified_devices=True,
        )
    except SendRetryError:
        logger.exception(f"Unable to send message response to {room_id}")


async def send_text_to_room(
    client, room_id, message, notice=True, markdown_convert=True
):
    """Send text to a matrix room

    Args:
        client (nio.AsyncClient): The client to communicate to matrix with

        room_id (str): The ID of the room to send the message to

        message (str): The message content

        notice (bool): Whether the message should be sent with an "m.notice" message type
            (will not ping users)

        markdown_convert (bool): Whether to convert the message content to markdown.
            Defaults to true.
    """
    # Determine whether to ping room members or not
    msgtype = "m.notice" if notice else "m.text"

    content = {
        "msgtype": msgtype,
        "format": "org.matrix.custom.html",
        "body": message,
    }

    if markdown_convert:
        content["formatted_body"] = markdown(message)

    try:
        await client.room_send(
            room_id,
            "m.room.message",
            content,
            ignore_unverified_devices=True,
        )
    except SendRetryError:
        logger.exception(f"Unable to send message response to {room_id}")


def mangle_giphy_url(url):
    m = re.search("(https://gph\.is/.*)", url)
    if m:
        r = requests.get(m.group(1), allow_redirects=False)
        location = r.headers["location"]
    else:
        location = url
    m = re.search("(https://media\.giphy\.com/media/.*\.gif)", location)
    if m:
        return m.group(1)
    m = re.search("https://giphy\.com/gifs/(.*)/html5", location)
    if m:
        return f"https://media.giphy.com/media/{m.group(1)}/giphy.gif"
    m = re.match("https://giphy.com/gifs/(.*-)?([A-Za-z0-9]+)$", location)
    if m:
        return f"https://media.giphy.com/media/{m.group(2)}/giphy.gif"
    m = re.search("https://imgur\.com/(gallery/)?(.*)", location)
    if m:
        return f"https://i.imgur.com/{m.group(2)}.png"
    return None


def fetch_file(url):
    try:
        r = requests.get(url, allow_redirects=True, headers={"user-agent": USER_AGENT})
        if r.status_code == requests.codes.ok:
            file = TemporaryFile()
            file.write(r.content)
            file.seek(0)
            return file, r.headers["content-type"]
    except requests.exceptions.ConnectionError as e:
        logger.error(e)
    return None, None


def fetch_ogdata(url):
    custom_tags = ["url", "title", "image", "description", "site_name"]
    try:
        return opengrapher.parse(url, parse_tags=custom_tags, user_agent=USER_AGENT)
    except Exception as e:
        logger.error(e)
    return {}


def format_ogdata(og):
    output = ""
    if "title" in og and "url" in og:
        output += f"> **[{og['title']}]({og['url']})**"
        if "site_name" in og:
            output += f"- {og['site_name']}"
    if "description" in og:
        output += f"\n\n> {og['description']}"
    return output


def get_random_emoji():
    aliases = emojis.db.get_emoji_aliases()
    return random.choice(list(aliases.values()))
